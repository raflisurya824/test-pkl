﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.HeadRegister = New System.Windows.Forms.Label()
        Me.password = New System.Windows.Forms.TextBox()
        Me.LabelPassword = New System.Windows.Forms.Label()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.username = New System.Windows.Forms.TextBox()
        Me.LabelUsername = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'HeadRegister
        '
        Me.HeadRegister.AutoSize = True
        Me.HeadRegister.Location = New System.Drawing.Point(81, 29)
        Me.HeadRegister.Name = "HeadRegister"
        Me.HeadRegister.Size = New System.Drawing.Size(56, 15)
        Me.HeadRegister.TabIndex = 11
        Me.HeadRegister.Text = "REGISTER"
        '
        'password
        '
        Me.password.Location = New System.Drawing.Point(93, 119)
        Me.password.Name = "password"
        Me.password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.password.Size = New System.Drawing.Size(100, 23)
        Me.password.TabIndex = 10
        '
        'LabelPassword
        '
        Me.LabelPassword.AutoSize = True
        Me.LabelPassword.Location = New System.Drawing.Point(22, 122)
        Me.LabelPassword.Name = "LabelPassword"
        Me.LabelPassword.Size = New System.Drawing.Size(57, 15)
        Me.LabelPassword.TabIndex = 9
        Me.LabelPassword.Text = "Password"
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(118, 163)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(75, 23)
        Me.btnLogin.TabIndex = 8
        Me.btnLogin.Text = "Register"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'username
        '
        Me.username.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.username.Location = New System.Drawing.Point(93, 75)
        Me.username.Name = "username"
        Me.username.Size = New System.Drawing.Size(100, 23)
        Me.username.TabIndex = 7
        '
        'LabelUsername
        '
        Me.LabelUsername.AutoSize = True
        Me.LabelUsername.Location = New System.Drawing.Point(22, 78)
        Me.LabelUsername.Name = "LabelUsername"
        Me.LabelUsername.Size = New System.Drawing.Size(60, 15)
        Me.LabelUsername.TabIndex = 6
        Me.LabelUsername.Text = "Username"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(217, 216)
        Me.Controls.Add(Me.HeadRegister)
        Me.Controls.Add(Me.password)
        Me.Controls.Add(Me.LabelPassword)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.username)
        Me.Controls.Add(Me.LabelUsername)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents HeadRegister As Label
    Friend WithEvents password As TextBox
    Friend WithEvents LabelPassword As Label
    Friend WithEvents btnLogin As Button
    Friend WithEvents username As TextBox
    Friend WithEvents LabelUsername As Label
End Class
