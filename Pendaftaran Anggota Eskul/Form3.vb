﻿Option Strict On
Option Explicit On

Imports Npgsql
Imports System.Data.SqlClient

Public Class Form3

    Dim connString As String = "Host=localhost;Port=5432;Database=perpustakaan;Username=postgres;Password=211721"
    Dim conn As New NpgsqlConnection(connString)
    Private ID As String = ""
    Public SQL As String = ""
    Public cmd As NpgsqlCommand
    Public intRow As Integer = 0

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            Dim dgv As DataGridView = DataGridView1
            If e.RowIndex <> -1 Then
                Me.ID = Convert.ToString(dgv.CurrentRow.Cells(0).Value).Trim()
                Button2.Text = "Update (" & Me.ID & ")"
                Button3.Text = "Delete (" & Me.ID & ")"

                TextBox1.Text = Convert.ToString(dgv.CurrentRow.Cells(1).Value).Trim()
                TextBox2.Text = Convert.ToString(dgv.CurrentRow.Cells(2).Value).Trim()
                TextBox3.Text = Convert.ToString(dgv.CurrentRow.Cells(3).Value).Trim()
                ComboBox1.SelectedItem = Convert.ToString(dgv.CurrentRow.Cells(3).Value).Trim()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub User4s_Click(sender As Object, e As EventArgs)

    End Sub


    Private Sub execute(Mysql As String, Optional Parameter As String = "")
        cmd = New NpgsqlCommand(Mysql, conn)
        AddParams(Parameter)
        CRUD(cmd)

    End Sub

    Private Sub AddParams(str As String)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("username", TextBox1.Text.Trim())
        cmd.Parameters.AddWithValue("kelas", TextBox2.Text.Trim())
        cmd.Parameters.AddWithValue("jurusan", ComboBox1.SelectedItem.ToString())
        cmd.Parameters.AddWithValue("ekstra", TextBox3.Text.Trim())

        If str = "Update" Or str = "Delete" And Not String.IsNullOrEmpty(Me.ID) Then
            cmd.Parameters.AddWithValue("ID", Me.ID)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles create.Click
        If String.IsNullOrEmpty(TextBox1.Text.Trim()) Or String.IsNullOrEmpty(TextBox2.Text.Trim()) Then

            MessageBox.Show("please input your biodata", "Error Insert Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Exit Sub
        End If

        SQL = "INSERT INTO anggota (username, kelas, jurusan, ekstra) VALUES(@username, @kelas, @jurusan, @ekstra)"
        execute(SQL, "insert")

        MessageBox.Show("success saving your biodata", "success Insert Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        LoadData()
    End Sub

    Private Sub LoadData(Optional keyword As String = "")
        SQL = "SELECT id, username, kelas, Jurusan, Ekstra FROM anggota"

        Dim staticKey As String = String.Format("%{0}%", keyword)
        cmd = New NpgsqlCommand(SQL, conn)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("keyword", staticKey)

        Dim dt As DataTable = CRUD(cmd)


        If dt.Rows.Count > 0 Then
            intRow = Convert.ToInt32(dt.Rows.Count.ToString())
        Else
            intRow = 0
        End If


        With DataGridView1
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = True
            .DataSource = dt

            .Columns(0).HeaderText = "ID"
            .Columns(1).HeaderText = "Username"
            .Columns(2).HeaderText = "kelas"
            .Columns(3).HeaderText = "jurusan"
            .Columns(4).HeaderText = "Ekstrakulikuler"

            .Columns(0).Width = 85
            .Columns(1).Width = 150
            .Columns(2).Width = 150
            .Columns(3).Width = 150
            .Columns(4).Width = 150

        End With
    End Sub

    Private Sub DataGridView1_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        If DataGridView1.Rows.Count = 0 Then

            MessageBox.Show("Please Select Your Biodata!", "success Update Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub

        End If

        If String.IsNullOrEmpty(Me.ID) Then
            MessageBox.Show("success saving your biodata", "success Update Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Exit Sub
        End If


        SQL = "UPDATE anggota SET Username = @username, kelas = @kelas, Jurusan = @jurusan, Ekstra = @ekstra WHERE id = @ID::integer"
        execute(SQL, "UPDATE")

        MessageBox.Show("success saving your biodata", "success Update Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        LoadData()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click


        If DataGridView1.Rows.Count = 0 Then

            MessageBox.Show("Please Inpur Your Biodata!!", "Failed Delete Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub

        End If

        If String.IsNullOrEmpty(Me.ID) Then
            MessageBox.Show("Please Select Your Biodata!", "Failed Delete Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Exit Sub
        End If


        SQL = " DELETE FROM anggota WHERE id = @ID::integer"
        execute(SQL, "DELETE")

        MessageBox.Show("success Delete your biodata", "success Delete Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        LoadData()


    End Sub

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub
End Class