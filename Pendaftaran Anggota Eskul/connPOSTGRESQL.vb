﻿Option Strict On
Option Explicit On

Imports Npgsql

Module connPOSTGRESQL
    Public Function getConn() As String
        Dim connString = "Host=localhost;Port=5432;Database=perpustakaan;Username=postgres;Password=211721"

        Return connString
    End Function

    Public conn As New NpgsqlConnection(getConn())
    Public cmd As NpgsqlCommand
    Public SQL As String = ""

    Public Function CRUD(conn As NpgsqlCommand) As DataTable

        Dim da As NpgsqlDataAdapter
        Dim dt As New DataTable()

        Try
            da = New NpgsqlDataAdapter
            da.SelectCommand = conn
            da.Fill(dt)

            Return dt
        Catch ex As Exception
            MessageBox.Show("An error in connection database" & ex.Message, "CRUD OPERATIONS FAILED", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dt = Nothing

        End Try

        Return dt

    End Function

End Module
